#include <avr/io.h>
#include "peri.h"

void init_peripheral(){
//  DDRC |= (1<<PC0) | (1<<PC1) | (1<<PC2);
//  PORTC &= ~(1<<PC0) & ~(1<<PC1) & ~(1<<PC2);

//  DDRC &= ~(1<<PC3);

//  PORTC |= (1<<PC3);
//  PORTC |= (1<<PC2);
//  PORTC |= (1<<PC1);
//  PORTC |= (1<<PC0);

//  PORTB |= (1<<PB3);
//  PORTB |= (1<<PB2);
//  PORTB |= (1<<PB1);
//  PORTB |= (1<<PB0);

//  DDRC &= ~(1<<PC4);
//  PORTC &= ~(1<<PC4);
    
    //DDRD &= ~(1<<PD0) & ~(1<<PD1) & ~(1<<PD5) & ~(1<<PD6);
    DDRB &= ~(1<<PB0) & ~(1<<PB1) & ~(1<<PB2) & ~(1<<PB3) & ~(1<<PB4) & ~(1<<PB5);
    
    DDRC &= ~(1<<PC0) & ~(1<<PC1) & ~(1<<PC2) & ~(1<<PC3);
    PORTC &= ~(1<<PC0) & ~(1<<PC1);

	PORTC |= (1<<PC2);
	PORTC |= (1<<PC3);

    PORTB |= (1<<PB5);
    PORTB |= (1<<PB4);
	PORTB |= (1<<PB3);
	PORTB |= (1<<PB2);
	PORTB |= (1<<PB1);
	PORTB |= (1<<PB0);
}

void set_led(uint8_t pin,uint8_t state){
	if(state)
		PORTC |= (1<<pin);
	else
		PORTC &= ~(1<<pin);
}

void set_led_value(uint8_t value){
	PORTC = value&0b00000111 | PORTC&0b11111000;
}

uint16_t read_adc(uint8_t channel){
	ADMUX = (0<<REFS1)|(1<<REFS0) | (0<<ADLAR) | (channel & 0b1111);
	ADCSRA = (1<<ADEN) | (1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0) | (1<<ADSC);
	while((ADCSRA & (1<<ADSC)));
	return ADC;
}
