package com.mygdx.gmgame;

public class StatusBar {
	public static final int PLAYER1 = 11;
	public static final int PLAYER2 = 22;
	public int playerTurn = 0;
	public int charge1 = 0;
	public int charge1T = 0;
	public int charge2 = 0;
	public int charge2T = 0;
	
	private World world;
	
	public StatusBar(World world){
		this.world = world;
	}
	
	public void update(){
		updateCharge1();
		updateCharge2();
	}
	
	public void updateCharge1(){
		if(world.getBullet1().getSpeed() == 0)
			charge1 = 0;
		else if(world.getBullet1().getSpeed() <= 1*Bullet.maxSpeed/50)
			charge1 = 1;
		else if(world.getBullet1().getSpeed() <= 2*Bullet.maxSpeed/50)
			charge1 = 2;
		else if(world.getBullet1().getSpeed() <= 3*Bullet.maxSpeed/50)
			charge1 = 3;
		else if(world.getBullet1().getSpeed() <= 4*Bullet.maxSpeed/50)
			charge1 = 4;
		else if(world.getBullet1().getSpeed() <= 5*Bullet.maxSpeed/50)
			charge1 = 5;
		else if(world.getBullet1().getSpeed() <= 6*Bullet.maxSpeed/50)
			charge1 = 6;
		else if(world.getBullet1().getSpeed() <= 7*Bullet.maxSpeed/50)
			charge1 = 7;
		else if(world.getBullet1().getSpeed() <= 8*Bullet.maxSpeed/50)
			charge1 = 8;
		else if(world.getBullet1().getSpeed() <= 9*Bullet.maxSpeed/50)
			charge1 = 9;
		else if(world.getBullet1().getSpeed() <= 10*Bullet.maxSpeed/50)
			charge1 = 10;
		
		else if(world.getBullet1().getSpeed() <= 11*Bullet.maxSpeed/50)
			charge1 = 11;
		else if(world.getBullet1().getSpeed() <= 12*Bullet.maxSpeed/50)
			charge1 = 12;
		else if(world.getBullet1().getSpeed() <= 13*Bullet.maxSpeed/50)
			charge1 = 13;
		else if(world.getBullet1().getSpeed() <= 14*Bullet.maxSpeed/50)
			charge1 = 14;
		else if(world.getBullet1().getSpeed() <= 15*Bullet.maxSpeed/50)
			charge1 = 15;
		else if(world.getBullet1().getSpeed() <= 16*Bullet.maxSpeed/50)
			charge1 = 16;
		else if(world.getBullet1().getSpeed() <= 17*Bullet.maxSpeed/50)
			charge1 = 17;
		else if(world.getBullet1().getSpeed() <= 18*Bullet.maxSpeed/50)
			charge1 = 18;
		else if(world.getBullet1().getSpeed() <= 19*Bullet.maxSpeed/50)
			charge1 = 19;
		else if(world.getBullet1().getSpeed() <= 20*Bullet.maxSpeed/50)
			charge1 = 20;
		
		else if(world.getBullet1().getSpeed() <= 21*Bullet.maxSpeed/50)
			charge1 = 21;
		else if(world.getBullet1().getSpeed() <= 22*Bullet.maxSpeed/50)
			charge1 = 22;
		else if(world.getBullet1().getSpeed() <= 23*Bullet.maxSpeed/50)
			charge1 = 23;
		else if(world.getBullet1().getSpeed() <= 24*Bullet.maxSpeed/50)
			charge1 = 24;
		else if(world.getBullet1().getSpeed() <= 25*Bullet.maxSpeed/50)
			charge1 = 25;
		else if(world.getBullet1().getSpeed() <= 26*Bullet.maxSpeed/50)
			charge1 = 26;
		else if(world.getBullet1().getSpeed() <= 27*Bullet.maxSpeed/50)
			charge1 = 27;
		else if(world.getBullet1().getSpeed() <= 28*Bullet.maxSpeed/50)
			charge1 = 28;
		else if(world.getBullet1().getSpeed() <= 29*Bullet.maxSpeed/50)
			charge1 = 29;
		else if(world.getBullet1().getSpeed() <= 30*Bullet.maxSpeed/50)
			charge1 = 30;
		
		else if(world.getBullet1().getSpeed() <= 31*Bullet.maxSpeed/50)
			charge1 = 31;
		else if(world.getBullet1().getSpeed() <= 32*Bullet.maxSpeed/50)
			charge1 = 32;
		else if(world.getBullet1().getSpeed() <= 33*Bullet.maxSpeed/50)
			charge1 = 33;
		else if(world.getBullet1().getSpeed() <= 34*Bullet.maxSpeed/50)
			charge1 = 34;
		else if(world.getBullet1().getSpeed() <= 35*Bullet.maxSpeed/50)
			charge1 = 35;
		else if(world.getBullet1().getSpeed() <= 36*Bullet.maxSpeed/50)
			charge1 = 36;
		else if(world.getBullet1().getSpeed() <= 37*Bullet.maxSpeed/50)
			charge1 = 37;
		else if(world.getBullet1().getSpeed() <= 38*Bullet.maxSpeed/50)
			charge1 = 38;
		else if(world.getBullet1().getSpeed() <= 39*Bullet.maxSpeed/50)
			charge1 = 39;
		else if(world.getBullet1().getSpeed() <= 40*Bullet.maxSpeed/50)
			charge1 = 40;
		
		else if(world.getBullet1().getSpeed() <= 41*Bullet.maxSpeed/50)
			charge1 = 41;
		else if(world.getBullet1().getSpeed() <= 42*Bullet.maxSpeed/50)
			charge1 = 42;
		else if(world.getBullet1().getSpeed() <= 43*Bullet.maxSpeed/50)
			charge1 = 43;
		else if(world.getBullet1().getSpeed() <= 44*Bullet.maxSpeed/50)
			charge1 = 44;
		else if(world.getBullet1().getSpeed() <= 45*Bullet.maxSpeed/50)
			charge1 = 45;
		else if(world.getBullet1().getSpeed() <= 46*Bullet.maxSpeed/50)
			charge1 = 46;
		else if(world.getBullet1().getSpeed() <= 47*Bullet.maxSpeed/50)
			charge1 = 47;
		else if(world.getBullet1().getSpeed() <= 48*Bullet.maxSpeed/50)
			charge1 = 48;
		else if(world.getBullet1().getSpeed() <= 49*Bullet.maxSpeed/50)
			charge1 = 49;
		else if(world.getBullet1().getSpeed() <= 50*Bullet.maxSpeed/50)
			charge1 = 50;
	}
	
	public void updateCharge2(){
		if(world.getBullet2().getSpeed() == 0)
			charge2 = 0;
		else if(world.getBullet2().getSpeed() <= 1*Bullet.maxSpeed/50)
			charge2 = 1;
		else if(world.getBullet2().getSpeed() <= 2*Bullet.maxSpeed/50)
			charge2 = 2;
		else if(world.getBullet2().getSpeed() <= 3*Bullet.maxSpeed/50)
			charge2 = 3;
		else if(world.getBullet2().getSpeed() <= 4*Bullet.maxSpeed/50)
			charge2 = 4;
		else if(world.getBullet2().getSpeed() <= 5*Bullet.maxSpeed/50)
			charge2 = 5;
		else if(world.getBullet2().getSpeed() <= 6*Bullet.maxSpeed/50)
			charge2 = 6;
		else if(world.getBullet2().getSpeed() <= 7*Bullet.maxSpeed/50)
			charge2 = 7;
		else if(world.getBullet2().getSpeed() <= 8*Bullet.maxSpeed/50)
			charge2 = 8;
		else if(world.getBullet2().getSpeed() <= 9*Bullet.maxSpeed/50)
			charge2 = 9;
		else if(world.getBullet2().getSpeed() <= 10*Bullet.maxSpeed/50)
			charge2 = 10;
		
		else if(world.getBullet2().getSpeed() <= 11*Bullet.maxSpeed/50)
			charge2 = 11;
		else if(world.getBullet2().getSpeed() <= 12*Bullet.maxSpeed/50)
			charge2 = 12;
		else if(world.getBullet2().getSpeed() <= 13*Bullet.maxSpeed/50)
			charge2 = 13;
		else if(world.getBullet2().getSpeed() <= 14*Bullet.maxSpeed/50)
			charge2 = 14;
		else if(world.getBullet2().getSpeed() <= 15*Bullet.maxSpeed/50)
			charge2 = 15;
		else if(world.getBullet2().getSpeed() <= 16*Bullet.maxSpeed/50)
			charge2 = 16;
		else if(world.getBullet2().getSpeed() <= 17*Bullet.maxSpeed/50)
			charge2 = 17;
		else if(world.getBullet2().getSpeed() <= 18*Bullet.maxSpeed/50)
			charge2 = 18;
		else if(world.getBullet2().getSpeed() <= 19*Bullet.maxSpeed/50)
			charge2 = 19;
		else if(world.getBullet2().getSpeed() <= 20*Bullet.maxSpeed/50)
			charge2 = 20;
		
		else if(world.getBullet2().getSpeed() <= 21*Bullet.maxSpeed/50)
			charge2 = 21;
		else if(world.getBullet2().getSpeed() <= 22*Bullet.maxSpeed/50)
			charge2 = 22;
		else if(world.getBullet2().getSpeed() <= 23*Bullet.maxSpeed/50)
			charge2 = 23;
		else if(world.getBullet2().getSpeed() <= 24*Bullet.maxSpeed/50)
			charge2 = 24;
		else if(world.getBullet2().getSpeed() <= 25*Bullet.maxSpeed/50)
			charge2 = 25;
		else if(world.getBullet2().getSpeed() <= 26*Bullet.maxSpeed/50)
			charge2 = 26;
		else if(world.getBullet2().getSpeed() <= 27*Bullet.maxSpeed/50)
			charge2 = 27;
		else if(world.getBullet2().getSpeed() <= 28*Bullet.maxSpeed/50)
			charge2 = 28;
		else if(world.getBullet2().getSpeed() <= 29*Bullet.maxSpeed/50)
			charge2 = 29;
		else if(world.getBullet2().getSpeed() <= 30*Bullet.maxSpeed/50)
			charge2 = 30;
		
		else if(world.getBullet2().getSpeed() <= 31*Bullet.maxSpeed/50)
			charge2 = 31;
		else if(world.getBullet2().getSpeed() <= 32*Bullet.maxSpeed/50)
			charge2 = 32;
		else if(world.getBullet2().getSpeed() <= 33*Bullet.maxSpeed/50)
			charge2 = 33;
		else if(world.getBullet2().getSpeed() <= 34*Bullet.maxSpeed/50)
			charge2 = 34;
		else if(world.getBullet2().getSpeed() <= 35*Bullet.maxSpeed/50)
			charge2 = 35;
		else if(world.getBullet2().getSpeed() <= 36*Bullet.maxSpeed/50)
			charge2 = 36;
		else if(world.getBullet2().getSpeed() <= 37*Bullet.maxSpeed/50)
			charge2 = 37;
		else if(world.getBullet2().getSpeed() <= 38*Bullet.maxSpeed/50)
			charge2 = 38;
		else if(world.getBullet2().getSpeed() <= 39*Bullet.maxSpeed/50)
			charge2 = 39;
		else if(world.getBullet2().getSpeed() <= 40*Bullet.maxSpeed/50)
			charge2 = 40;
		
		else if(world.getBullet2().getSpeed() <= 41*Bullet.maxSpeed/50)
			charge2 = 41;
		else if(world.getBullet2().getSpeed() <= 42*Bullet.maxSpeed/50)
			charge2 = 42;
		else if(world.getBullet2().getSpeed() <= 43*Bullet.maxSpeed/50)
			charge2 = 43;
		else if(world.getBullet2().getSpeed() <= 44*Bullet.maxSpeed/50)
			charge2 = 44;
		else if(world.getBullet2().getSpeed() <= 45*Bullet.maxSpeed/50)
			charge2 = 45;
		else if(world.getBullet2().getSpeed() <= 46*Bullet.maxSpeed/50)
			charge2 = 46;
		else if(world.getBullet2().getSpeed() <= 47*Bullet.maxSpeed/50)
			charge2 = 47;
		else if(world.getBullet2().getSpeed() <= 48*Bullet.maxSpeed/50)
			charge2 = 48;
		else if(world.getBullet2().getSpeed() <= 49*Bullet.maxSpeed/50)
			charge2 = 49;
		else if(world.getBullet2().getSpeed() <= 50*Bullet.maxSpeed/50)
			charge2 = 50;
	}
}
