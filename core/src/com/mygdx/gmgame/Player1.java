package com.mygdx.gmgame;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.Vector2;


public class Player1 extends Player {
	public static int life = 3;
    public Player1(int x, int y, World world) {
    	position = new Vector2(x, y);
    	this.world = world;
    }
    
    public void update(){
    	if(Gdx.input.isKeyJustPressed(Keys.W)){
    		onJump();
    	}
    	if(world.getPeri().getSwitcha2()){
    		onJump();
    	}
    		
    	if(Gdx.input.isKeyPressed(Keys.A)){
            status = LEFT;
    		if(canMoveInDirection()) {
    			position.x -= speed;
    		}
    	}
    	if(world.getPeri().getSwitcha0()){
            status = LEFT;
    		if(canMoveInDirection()) {
    			position.x -= speed;
    		}
    	}
    	
    	if(Gdx.input.isKeyPressed(Keys.D)){
    		status = RIGHT;
    		if(canMoveInDirection()) {
    			position.x += speed;
    		}
    	}
    	if(world.getPeri().getSwitcha1()){
    		status = RIGHT;
    		if(canMoveInDirection()) {
    			position.x += speed;
    		}
    	}
    	
    	onGravity();
    }
}