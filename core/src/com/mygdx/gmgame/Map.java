package com.mygdx.gmgame;

public class Map {
	public StringBuilder[] MapData;
	public static int height;
	public static int width;
	private World world;
	
	public Map(World world) {	
		 MapData = new StringBuilder[32];
		 MapData[0] = new StringBuilder("##################################################");
		 MapData[1] = new StringBuilder("#................................................#");
         MapData[2] = new StringBuilder("#................................................#");
         MapData[3] = new StringBuilder("#................................................#");
         MapData[4] = new StringBuilder("#................................................#");
         MapData[5] = new StringBuilder("#................................................#");
         MapData[6] = new StringBuilder("#.................#...........#..................#");
         MapData[7] = new StringBuilder("#................##...........##.................#");
		 MapData[8] = new StringBuilder("#..xxxxxx.......###..xx...xx..###........xxxxxx..#");
         MapData[9] = new StringBuilder("#..............####...........####...............#");
        MapData[10] = new StringBuilder("#.............#####...........#####..............#");
        MapData[11] = new StringBuilder("#............######....xxx....######.............#");
        MapData[12] = new StringBuilder("#..........########...........#######............#");
        MapData[13] = new StringBuilder("#.........#########...........########...........#");
        MapData[14] = new StringBuilder("#........##########xx.......xx#########..........#");
		MapData[15] = new StringBuilder("#................................................#");
        MapData[16] = new StringBuilder("#xx##.......................................##xxx#");
        MapData[17] = new StringBuilder("#................................................#");
        MapData[18] = new StringBuilder("#..........####xxxxxxxxxxxxxxxxxxxx####..........#");
        MapData[19] = new StringBuilder("#..................xxxxxxxxxxx...................#");
        MapData[20] = new StringBuilder("#................................................#");
        MapData[21] = new StringBuilder("#xxx##.....................................##xxxx#");
		MapData[22] = new StringBuilder("#................................................#");
        MapData[23] = new StringBuilder("#.........###xxxx................xxxx###.........#");
        MapData[24] = new StringBuilder("#............xx........xxxx........xx............#");
        MapData[25] = new StringBuilder("#............xx.........xx.........xx............#");
        MapData[26] = new StringBuilder("#............xx.........xx.........xx............#");
        MapData[27] = new StringBuilder("#............xx.........xx.........xx............#");
        MapData[28] = new StringBuilder("#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx#");
		MapData[29] = new StringBuilder("#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx#");
        MapData[30] = new StringBuilder("#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx#");
        MapData[31] = new StringBuilder("##################################################");
        height = MapData.length;
		width = MapData[0].length();
		this.world = world;
	}
	
	public int getHeight() {
		return height;
	}
	
	public int getWidth() {
		return width;
	}

	public boolean hasWallAt(int r , int c) {
//		System.out.println("map    " + r + " " + c);
//		if(r >= 0 && r <= height && c >= 0 && c <= width)
			return MapData[r].charAt(c) == '#' || MapData[r].charAt(c) == 'x';
//		return false;
	}
	
	public boolean canBulletHit(int r , int c, int type) {
		if(r >= 0 && r < height && c >= 0 && c < width)
			return MapData[r].charAt(c) == '#' || MapData[r].charAt(c) == 'x';
		else{
			switch(type){
			case Bullet.PLAYER1:
				world.getBullet1().fired = false;
			case Bullet.PLAYER2:
				world.getBullet2().fired = false;
			}
		}
			
		return false;
	}
	
	public boolean onFloor(int r , int c) {
//		System.out.println(r + " " + c);
//		if(r >= 0 && r <= height && c >= 0 && c <= width)
			return MapData[r+1].charAt(c) == '#' || MapData[r+1].charAt(c) == 'x';
//		return false;
	}
	
	public void deleteBlock(int r, int c) {
		if(MapData[r].charAt(c) == 'x') {
			MapData[r].setCharAt(c, '.');
		}
	}
	
	public void resetMap() {
		MapData[0] = new StringBuilder("##################################################");
		MapData[1] = new StringBuilder("#................................................#");
        MapData[2] = new StringBuilder("#................................................#");
        MapData[3] = new StringBuilder("#................................................#");
        MapData[4] = new StringBuilder("#................................................#");
        MapData[5] = new StringBuilder("#................................................#");
        MapData[6] = new StringBuilder("#.................#...........#..................#");
        MapData[7] = new StringBuilder("#................##...........##.................#");
		MapData[8] = new StringBuilder("#..xxxxxx.......###..xx...xx..###........xxxxxx..#");
        MapData[9] = new StringBuilder("#..............####...........####...............#");
       MapData[10] = new StringBuilder("#.............#####...........#####..............#");
       MapData[11] = new StringBuilder("#............######....xxx....######.............#");
       MapData[12] = new StringBuilder("#..........########...........#######............#");
       MapData[13] = new StringBuilder("#.........#########...........########...........#");
       MapData[14] = new StringBuilder("#........##########xx.......xx#########..........#");
       MapData[15] = new StringBuilder("#................................................#");
       MapData[16] = new StringBuilder("#xx##.......................................##xxx#");
       MapData[17] = new StringBuilder("#................................................#");
       MapData[18] = new StringBuilder("#..........####xxxxxxxxxxxxxxxxxxxx####..........#");
       MapData[19] = new StringBuilder("#..................xxxxxxxxxxx...................#");
       MapData[20] = new StringBuilder("#................................................#");
       MapData[21] = new StringBuilder("#xxx##.....................................##xxxx#");
       MapData[22] = new StringBuilder("#................................................#");
       MapData[23] = new StringBuilder("#.........###xxxx................xxxx###.........#");
       MapData[24] = new StringBuilder("#............xx........xxxx........xx............#");
       MapData[25] = new StringBuilder("#............xx.........xx.........xx............#");
       MapData[26] = new StringBuilder("#............xx.........xx.........xx............#");
       MapData[27] = new StringBuilder("#............xx.........xx.........xx............#");
       MapData[28] = new StringBuilder("#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx#");
       MapData[29] = new StringBuilder("#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx#");
       MapData[30] = new StringBuilder("#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx#");
       MapData[31] = new StringBuilder("##################################################");
	}

}