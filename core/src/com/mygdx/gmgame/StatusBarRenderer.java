package com.mygdx.gmgame;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class StatusBarRenderer {
	private Texture player1Fired = new Texture("player1Fired.png");
	private Texture player2Fired = new Texture("player2Fired.png");
	private Texture GMgame = new Texture("GMgame.png");
	
	private Texture charge0 = new Texture("chargeV0.png");
	private Texture charge1 = new Texture("chargeV1.png");
	private Texture charge2 = new Texture("chargeV2.png");
	private Texture charge3 = new Texture("chargeV3.png");
	private Texture charge4 = new Texture("chargeV4.png");
	private Texture charge5 = new Texture("chargeV5.png");
	private Texture charge6 = new Texture("chargeV6.png");
	private Texture charge7 = new Texture("chargeV7.png");
	private Texture charge8 = new Texture("chargeV8.png");
	private Texture charge9 = new Texture("chargeV9.png");
	private Texture charge10 = new Texture("chargeV10.png");
	private Texture charge11 = new Texture("chargeV11.png");
	private Texture charge12 = new Texture("chargeV12.png");
	private Texture charge13 = new Texture("chargeV13.png");
	private Texture charge14 = new Texture("chargeV14.png");
	private Texture charge15 = new Texture("chargeV15.png");
	private Texture charge16 = new Texture("chargeV16.png");
	private Texture charge17 = new Texture("chargeV17.png");
	private Texture charge18 = new Texture("chargeV18.png");
	private Texture charge19 = new Texture("chargeV19.png");
	private Texture charge20 = new Texture("chargeV20.png");
	private Texture charge21 = new Texture("chargeV21.png");
	private Texture charge22 = new Texture("chargeV22.png");
	private Texture charge23 = new Texture("chargeV23.png");
	private Texture charge24 = new Texture("chargeV24.png");
	private Texture charge25 = new Texture("chargeV25.png");
	private Texture charge26 = new Texture("chargeV26.png");
	private Texture charge27 = new Texture("chargeV27.png");
	private Texture charge28 = new Texture("chargeV28.png");
	private Texture charge29 = new Texture("chargeV29.png");
	private Texture charge30 = new Texture("chargeV30.png");
	private Texture charge31 = new Texture("chargeV31.png");
	private Texture charge32 = new Texture("chargeV32.png");
	private Texture charge33 = new Texture("chargeV33.png");
	private Texture charge34 = new Texture("chargeV34.png");
	private Texture charge35 = new Texture("chargeV35.png");
	private Texture charge36 = new Texture("chargeV36.png");
	private Texture charge37 = new Texture("chargeV37.png");
	private Texture charge38 = new Texture("chargeV38.png");
	private Texture charge39 = new Texture("chargeV39.png");
	private Texture charge40 = new Texture("chargeV40.png");
	private Texture charge41 = new Texture("chargeV41.png");
	private Texture charge42 = new Texture("chargeV42.png");
	private Texture charge43 = new Texture("chargeV43.png");
	private Texture charge44 = new Texture("chargeV44.png");
	private Texture charge45 = new Texture("chargeV45.png");
	private Texture charge46 = new Texture("chargeV46.png");
	private Texture charge47 = new Texture("chargeV47.png");
	private Texture charge48 = new Texture("chargeV48.png");
	private Texture charge49 = new Texture("chargeV49.png");
	private Texture charge50 = new Texture("chargeV50.png");
	private World world;
	private SpriteBatch batch;
	
	public StatusBarRenderer(World world){
		this.world = world;
		this.batch = world.getMyGame().batch;
	}
	
	public void render(float delta){
		renderTurnBar();
		renderChargePlayer1();
		renderChargePlayer2();
	}
	
	private void renderTurnBar(){
		if(world.getStatusBar().playerTurn == StatusBar.PLAYER1)
			batch.draw(player1Fired, 0, 0);
		else if(world.getStatusBar().playerTurn == StatusBar.PLAYER2)
			batch.draw(player2Fired, 0, 0);
		else
			batch.draw(GMgame, 0, 0);
	}
	
	private void renderChargePlayer1(){
		if(world.getStatusBar().charge1 == 0)
			batch.draw(charge0, 200, 0);
		if(world.getStatusBar().charge1 == 1)
			batch.draw(charge1, 200, 0);
		if(world.getStatusBar().charge1 == 2)
			batch.draw(charge2, 200, 0);
		if(world.getStatusBar().charge1 == 3)
			batch.draw(charge3, 200, 0);
		if(world.getStatusBar().charge1 == 4)
			batch.draw(charge4, 200, 0);
		if(world.getStatusBar().charge1 == 5)
			batch.draw(charge5, 200, 0);
		if(world.getStatusBar().charge1 == 6)
			batch.draw(charge6, 200, 0);
		if(world.getStatusBar().charge1 == 7)
			batch.draw(charge7, 200, 0);
		if(world.getStatusBar().charge1 == 8)
			batch.draw(charge8, 200, 0);
		if(world.getStatusBar().charge1 == 9)
			batch.draw(charge9, 200, 0);
		if(world.getStatusBar().charge1 == 10)
			batch.draw(charge10, 200, 0);
		
		if(world.getStatusBar().charge1 == 11)
			batch.draw(charge11, 200, 0);
		if(world.getStatusBar().charge1 == 12)
			batch.draw(charge12, 200, 0);
		if(world.getStatusBar().charge1 == 13)
			batch.draw(charge13, 200, 0);
		if(world.getStatusBar().charge1 == 14)
			batch.draw(charge14, 200, 0);
		if(world.getStatusBar().charge1 == 15)
			batch.draw(charge15, 200, 0);
		if(world.getStatusBar().charge1 == 16)
			batch.draw(charge16, 200, 0);
		if(world.getStatusBar().charge1 == 17)
			batch.draw(charge17, 200, 0);
		if(world.getStatusBar().charge1 == 18)
			batch.draw(charge18, 200, 0);
		if(world.getStatusBar().charge1 == 19)
			batch.draw(charge19, 200, 0);
		if(world.getStatusBar().charge1 == 20)
			batch.draw(charge20, 200, 0);

		if(world.getStatusBar().charge1 == 21)
			batch.draw(charge21, 200, 0);
		if(world.getStatusBar().charge1 == 22)
			batch.draw(charge22, 200, 0);
		if(world.getStatusBar().charge1 == 23)
			batch.draw(charge23, 200, 0);
		if(world.getStatusBar().charge1 == 24)
			batch.draw(charge24, 200, 0);
		if(world.getStatusBar().charge1 == 25)
			batch.draw(charge25, 200, 0);
		if(world.getStatusBar().charge1 == 26)
			batch.draw(charge26, 200, 0);
		if(world.getStatusBar().charge1 == 27)
			batch.draw(charge27, 200, 0);
		if(world.getStatusBar().charge1 == 28)
			batch.draw(charge28, 200, 0);
		if(world.getStatusBar().charge1 == 29)
			batch.draw(charge29, 200, 0);
		if(world.getStatusBar().charge1 == 30)
			batch.draw(charge30, 200, 0);
		
		if(world.getStatusBar().charge1 == 31)
			batch.draw(charge31, 200, 0);
		if(world.getStatusBar().charge1 == 32)
			batch.draw(charge32, 200, 0);
		if(world.getStatusBar().charge1 == 33)
			batch.draw(charge33, 200, 0);
		if(world.getStatusBar().charge1 == 34)
			batch.draw(charge34, 200, 0);
		if(world.getStatusBar().charge1 == 35)
			batch.draw(charge35, 200, 0);
		if(world.getStatusBar().charge1 == 36)
			batch.draw(charge36, 200, 0);
		if(world.getStatusBar().charge1 == 37)
			batch.draw(charge37, 200, 0);
		if(world.getStatusBar().charge1 == 38)
			batch.draw(charge38, 200, 0);
		if(world.getStatusBar().charge1 == 39)
			batch.draw(charge39, 200, 0);
		if(world.getStatusBar().charge1 == 40)
			batch.draw(charge40, 200, 0);
		
		if(world.getStatusBar().charge1 == 41)
			batch.draw(charge41, 200, 0);
		if(world.getStatusBar().charge1 == 42)
			batch.draw(charge42, 200, 0);
		if(world.getStatusBar().charge1 == 43)
			batch.draw(charge43, 200, 0);
		if(world.getStatusBar().charge1 == 44)
			batch.draw(charge44, 200, 0);
		if(world.getStatusBar().charge1 == 45)
			batch.draw(charge45, 200, 0);
		if(world.getStatusBar().charge1 == 46)
			batch.draw(charge46, 200, 0);
		if(world.getStatusBar().charge1 == 47)
			batch.draw(charge47, 200, 0);
		if(world.getStatusBar().charge1 == 48)
			batch.draw(charge48, 200, 0);
		if(world.getStatusBar().charge1 == 49)
			batch.draw(charge49, 200, 0);
		if(world.getStatusBar().charge1 == 50)
			batch.draw(charge50, 200, 0);
	}
	
	private void renderChargePlayer2(){
		if(world.getStatusBar().charge2 == 0)
			batch.draw(charge0, 450, 0);
		if(world.getStatusBar().charge2 == 1)
			batch.draw(charge1, 450, 0);
		if(world.getStatusBar().charge2 == 2)
			batch.draw(charge2, 450, 0);
		if(world.getStatusBar().charge2 == 3)
			batch.draw(charge3, 450, 0);
		if(world.getStatusBar().charge2 == 4)
			batch.draw(charge4, 450, 0);
		if(world.getStatusBar().charge2 == 5)
			batch.draw(charge5, 450, 0);
		if(world.getStatusBar().charge2 == 6)
			batch.draw(charge6, 450, 0);
		if(world.getStatusBar().charge2 == 7)
			batch.draw(charge7, 450, 0);
		if(world.getStatusBar().charge2 == 8)
			batch.draw(charge8, 450, 0);
		if(world.getStatusBar().charge2 == 9)
			batch.draw(charge9, 450, 0);
		if(world.getStatusBar().charge2 == 10)
			batch.draw(charge10, 450, 0);
		
		if(world.getStatusBar().charge2 == 11)
			batch.draw(charge11, 450, 0);
		if(world.getStatusBar().charge2 == 12)
			batch.draw(charge12, 450, 0);
		if(world.getStatusBar().charge2 == 13)
			batch.draw(charge13, 450, 0);
		if(world.getStatusBar().charge2 == 14)
			batch.draw(charge14, 450, 0);
		if(world.getStatusBar().charge2 == 15)
			batch.draw(charge15, 450, 0);
		if(world.getStatusBar().charge2 == 16)
			batch.draw(charge16, 450, 0);
		if(world.getStatusBar().charge2 == 17)
			batch.draw(charge17, 450, 0);
		if(world.getStatusBar().charge2 == 18)
			batch.draw(charge18, 450, 0);
		if(world.getStatusBar().charge2 == 19)
			batch.draw(charge19, 450, 0);
		if(world.getStatusBar().charge2 == 20)
			batch.draw(charge20, 450, 0);

		if(world.getStatusBar().charge2 == 21)
			batch.draw(charge21, 450, 0);
		if(world.getStatusBar().charge2 == 22)
			batch.draw(charge22, 450, 0);
		if(world.getStatusBar().charge2 == 23)
			batch.draw(charge23, 450, 0);
		if(world.getStatusBar().charge2 == 24)
			batch.draw(charge24, 450, 0);
		if(world.getStatusBar().charge2 == 25)
			batch.draw(charge25, 450, 0);
		if(world.getStatusBar().charge2 == 26)
			batch.draw(charge26, 450, 0);
		if(world.getStatusBar().charge2 == 27)
			batch.draw(charge27, 450, 0);
		if(world.getStatusBar().charge2 == 28)
			batch.draw(charge28, 450, 0);
		if(world.getStatusBar().charge2 == 29)
			batch.draw(charge29, 450, 0);
		if(world.getStatusBar().charge2 == 30)
			batch.draw(charge30, 450, 0);
		
		if(world.getStatusBar().charge2 == 31)
			batch.draw(charge31, 450, 0);
		if(world.getStatusBar().charge2 == 32)
			batch.draw(charge32, 450, 0);
		if(world.getStatusBar().charge2 == 33)
			batch.draw(charge33, 450, 0);
		if(world.getStatusBar().charge2 == 34)
			batch.draw(charge34, 450, 0);
		if(world.getStatusBar().charge2 == 35)
			batch.draw(charge35, 450, 0);
		if(world.getStatusBar().charge2 == 36)
			batch.draw(charge36, 450, 0);
		if(world.getStatusBar().charge2 == 37)
			batch.draw(charge37, 450, 0);
		if(world.getStatusBar().charge2 == 38)
			batch.draw(charge38, 450, 0);
		if(world.getStatusBar().charge2 == 39)
			batch.draw(charge39, 450, 0);
		if(world.getStatusBar().charge2 == 40)
			batch.draw(charge40, 450, 0);
		
		if(world.getStatusBar().charge2 == 41)
			batch.draw(charge41, 450, 0);
		if(world.getStatusBar().charge2 == 42)
			batch.draw(charge42, 450, 0);
		if(world.getStatusBar().charge2 == 43)
			batch.draw(charge43, 450, 0);
		if(world.getStatusBar().charge2 == 44)
			batch.draw(charge44, 450, 0);
		if(world.getStatusBar().charge2 == 45)
			batch.draw(charge45, 450, 0);
		if(world.getStatusBar().charge2 == 46)
			batch.draw(charge46, 450, 0);
		if(world.getStatusBar().charge2 == 47)
			batch.draw(charge47, 450, 0);
		if(world.getStatusBar().charge2 == 48)
			batch.draw(charge48, 450, 0);
		if(world.getStatusBar().charge2 == 49)
			batch.draw(charge49, 450, 0);
		if(world.getStatusBar().charge2 == 50)
			batch.draw(charge50, 450, 0);
			
	}
}
