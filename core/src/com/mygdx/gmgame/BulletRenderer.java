package com.mygdx.gmgame;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class BulletRenderer {
	private World world;
	private SpriteBatch batch;
	private Texture bulletRight = new Texture("bulletRight40.png");
	private Texture bulletLeft = new Texture("bulletLeft40.png");
	private TextureRegion bulletRightR = new TextureRegion(bulletRight);
	private TextureRegion bulletLeftR = new TextureRegion(bulletLeft);
	private int bulletWidthReal = 40;
	private int bulletHeightReal = 26;
	private int bulletWidth = bulletWidthReal*7/8;
	private int bulletHeight = bulletHeightReal*7/8;
	private Texture bulletP1 = new Texture("redball.png");
	private Texture bulletP2 = new Texture("blueball.png");
	private Bullet1 bullet1;
	private Bullet2 bullet2;
	
	public BulletRenderer(World world){
		this.world = world;
		this.batch = world.getMyGame().batch;
		this.bullet1 = world.getBullet1();
		this.bullet2 = world.getBullet2();
	}
	
	int x1, y1, x2, y2;
	public void render(float delta){
		if(bullet1.isVisible) {
			x1 = (int) world.getBullet1().getPosition().x;
			y1 = (int) world.getBullet1().getPosition().y;
			switch(bullet1.direction){
				case Player.RIGHT:
//					batch.draw(bulletRight, x1, MyGame.HEIGHT - y1 - 40);
					batch.draw(bulletRightR, x1, MyGame.HEIGHT - y1 - 40, bulletWidth/2, bulletHeight/2, bulletWidth, bulletHeight, 1, 1, 0);
					break;
				case Player.LEFT:
//					batch.draw(bulletLeft, x1 - 40, MyGame.HEIGHT - y1 - 40);
					batch.draw(bulletLeftR, x1 - 20, MyGame.HEIGHT - y1 - 40, bulletWidth/2, bulletHeight/2, bulletWidth, bulletHeight, 1, 1, 0);
			}
			batch.draw(bulletP1, x1, MyGame.HEIGHT - y1 - 40);
		}
		if(bullet2.isVisible) {
			x2 = (int) world.getBullet2().getPosition().x;
			y2 = (int) world.getBullet2().getPosition().y;
			switch(bullet2.direction){
				case Player.RIGHT:
	//				batch.draw(bulletRight, x2, MyGame.HEIGHT - y2 - 40);
					batch.draw(bulletRightR, x2, MyGame.HEIGHT - y2 - 40, bulletWidth/2, bulletHeight/2, bulletWidth, bulletHeight, 1, 1, 0);
					break;
				case Player.LEFT:
	//				batch.draw(bulletLeft, x2 - 40, MyGame.HEIGHT - y2 - 40);
					batch.draw(bulletLeftR, x2 - 20, MyGame.HEIGHT - y2 - 40, bulletWidth/2, bulletHeight/2, bulletWidth, bulletHeight, 1, 1, 0);
			}
			batch.draw(bulletP2, x2, MyGame.HEIGHT - y2 - 40);
		}
	}
}